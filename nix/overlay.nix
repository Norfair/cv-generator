final: prev:
with final.lib;
with final.haskell.lib;

{
  cvRelease =
    let
      latexenv = final.texlive.combine
        {
          inherit
            (final.texlive)
            scheme-full
            latexmk
            libertine
            ;
        };

    in
    final.stdenv.mkDerivation {
      name = "cv";
      buildInputs = [
        (justStaticExecutables final.haskellPackages.cv-generator)
        final.pdftk
        latexenv
      ];
      buildCommand = ''
        mkdir -p $out
        cv-generator
        cp auto/*/*.pdf $out
      '';
      passthru = {
        inherit latexenv;
      };
    };

  haskellPackages = prev.haskellPackages.override (old: {
    overrides = composeExtensions (old.overrides or (_: _: { })) (
      self: super: {
        cv-generator = buildStrictly (self.callPackage ../cv-generator { });
      }
    );
  });
}
