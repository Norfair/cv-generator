{ mkDerivation, aeson, aeson-casing, autodocodec, base, bytestring
, hashable, lib, path, path-io, shakespeare, template-haskell, text
, typed-process, yaml
}:
mkDerivation {
  pname = "cv-generator";
  version = "0.0.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson aeson-casing autodocodec base bytestring hashable path
    path-io shakespeare template-haskell text typed-process yaml
  ];
  executableHaskellDepends = [ base ];
  license = lib.licenses.unfree;
  hydraPlatforms = lib.platforms.none;
  mainProgram = "cv-generator";
}
