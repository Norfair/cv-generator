{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskellQuotes #-}
{-# LANGUAGE TypeApplications #-}

module CV.Generator.Widget where

import Data.Text (Text)
import qualified Data.Text.Lazy as TL
import Data.Text.Lazy.Builder (Builder, toLazyText)
import qualified Data.Text.Lazy.Builder as Text
import Data.Typeable
import Data.Yaml as Yaml
import Language.Haskell.TH.Quote
import Language.Haskell.TH.Syntax
import Text.Shakespeare
import Text.Shakespeare.Text

getSets :: Q ShakespeareSettings
getSets = do
  toTExp <- [|toText|]
  wrapExp <- [|id|]
  unWrapExp <- [|id|]
  pure $
    defaultShakespeareSettings
      { toBuilder = toTExp,
        wrap = wrapExp,
        unwrap = unWrapExp,
        varChar = '~',
        urlChar = '\NUL',
        intChar = '^',
        justVarInterpolation = False
      }

texFile :: FilePath -> Q Exp
texFile fp = do
  sets <- getSets
  let fp' = "templates/" ++ fp <> ".tex"
  qAddDependentFile fp'
  shakespeareFile sets fp'

completeTemplate :: (t -> Builder) -> Text
completeTemplate w = toStrictText $ w undefined

toStrictText :: Builder -> Text
toStrictText = TL.toStrict . toLazyText

tex :: QuasiQuoter
tex =
  QuasiQuoter
    { quoteExp = \s -> do
        rs <- getSets
        shakespeareFromString rs {justVarInterpolation = False} s,
      quotePat = undefined,
      quoteType = undefined,
      quoteDec = undefined
    }

type Widget = RenderUrl () -> Text.Builder

staticYaml :: forall dat. (Typeable dat, FromJSON dat, Lift dat) => String -> Q [Dec]
staticYaml name = do
  let n = mkName name
  e <- yamlFile @dat name
  pure
    [ -- SigD n $ ConT ''dat,
      FunD
        n
        [ Clause [] (NormalB e) []
        ]
    ]

yamlFile :: forall dat. (FromJSON dat, Lift dat) => FilePath -> Q Exp
yamlFile fp = do
  let fp' = "data/" <> fp <> ".yaml"
  qAddDependentFile fp'
  dat <- runIO $ do
    putStrLn fp'
    Yaml.decodeFileThrow @IO @dat fp'
  lift dat
