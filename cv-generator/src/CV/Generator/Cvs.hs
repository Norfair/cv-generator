{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module CV.Generator.Cvs where

import CV.Generator.Pieces
import CV.Generator.Pieces.Types
import CV.Generator.Widget

allCvs :: [(FilePath, CV)]
allCvs =
  [ ("cv", normalCv),
    ("fail", failCv),
    ("complete", completeCv),
    ("technical-leader", technicalLeaderCv),
    ("individual-contributor", individualContributorCv)
  ]

data CV = CV
  { cvWidget :: Widget,
    cvLengthCheck :: Maybe Word
  }

normalCv :: CV
normalCv = CV {cvWidget = $(texFile "normal"), cvLengthCheck = Just 2}

failCv :: CV
failCv = CV {cvWidget = $(texFile "fail"), cvLengthCheck = Nothing}

completeCv :: CV
completeCv = CV {cvWidget = $(texFile "complete"), cvLengthCheck = Nothing}

technicalLeaderCv :: CV
technicalLeaderCv = CV {cvWidget = $(texFile "technical-leader"), cvLengthCheck = Just 2}

individualContributorCv :: CV
individualContributorCv = CV {cvWidget = $(texFile "individual-contributor"), cvLengthCheck = Just 2}
