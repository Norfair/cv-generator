{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
-- {-# OPTIONS_GHC -ddump-splices #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

module CV.Generator.Pieces where

import CV.Generator.Pieces.Types
import CV.Generator.Widget
import Data.Bool
import Data.List
import Data.Maybe
import Data.Ord
import Data.Text (Text)
import qualified Data.Text as T

texHeaderW :: Widget
texHeaderW = $(texFile "tex-header")

itemW :: Text -> Widget
itemW t = [tex|\item ~{t}|]

tightemsW :: [Text] -> Widget
tightemsW [] = mempty
tightemsW ts =
  [tex|
    \begin{tightemize}
      ^{foldMap itemW ts}
    \end{tightemize}
  |]

tightcolemsW :: [Text] -> Widget
tightcolemsW [] = mempty
tightcolemsW ts =
  [tex|
    \begin{tightcolitemize}
      ^{foldMap itemW ts}
    \end{tightcolitemize}
  |]

$(staticYaml @Basics "basics")

headingW :: Widget
headingW = $(texFile "heading")

$(staticYaml @[Text] "keywords")

$(staticYaml @[Text] "technologies")

keywordsW :: Widget
keywordsW = $(texFile "keywords")

$(staticYaml @[Book] "books")

booksW :: Widget
booksW = $(texFile "books")

$(staticYaml @[Talk] "talks")

talkW :: Talk -> Widget
talkW t = $(texFile "talk")

talksW :: Widget
talksW = $(texFile "talks")

speakingW :: Widget
speakingW = $(texFile "speaking")

$(staticYaml @[Failure] "failures")

failureW :: Failure -> Widget
failureW f = $(texFile "failure")

failuresW :: Widget
failuresW = $(texFile "failures")

$(staticYaml @[Experience] "experiences")

experienceW :: Experience -> Widget
experienceW e = if fromMaybe False (experienceHidden e) then mempty else $(texFile "experience")

experiencesW :: Widget
experiencesW = $(texFile "experiences")

experienceForRecruitersW :: Experience -> Widget
experienceForRecruitersW e = $(texFile "experience-for-recruiters")

experiencesForRecruitersW :: Widget
experiencesForRecruitersW = $(texFile "experiences-for-recruiters")

experiencesTechnicalLeaderW :: Widget
experiencesTechnicalLeaderW = $(texFile "experiences-technical-leader")

experienceTechnicalLeaderW :: Experience -> Widget
experienceTechnicalLeaderW e = $(texFile "experience-technical-leader")

experiencesIndividualContributorW :: Widget
experiencesIndividualContributorW = $(texFile "experiences-individual-contributor")

experienceIndividualContributorW :: Experience -> Widget
experienceIndividualContributorW e = $(texFile "experience-individual-contributor")

$(staticYaml @[Education] "educations")

educationW :: Education -> Widget
educationW e = $(texFile "education")

educationsW :: Widget
educationsW = $(texFile "educations")

$(staticYaml @[Product] "products")

productW :: Product -> Widget
productW p = $(texFile "product")

productsW :: Widget
productsW = $(texFile "products")

$(staticYaml @[Software] "softwares")

softwareW :: Software -> Widget
softwareW s = $(texFile "software")

softwaresW :: Widget
softwaresW = $(texFile "softwares")

$(staticYaml @[Language] "languages")

languageW :: Language -> Widget
languageW l = $(texFile "language")

languagesW :: Widget
languagesW = $(texFile "languages")
