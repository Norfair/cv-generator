{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveLift #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE OverloadedStrings #-}

module CV.Generator.Pieces.Types where

import Autodocodec
import Data.Aeson (FromJSON, ToJSON)
import Data.Text (Text)
import GHC.Generics (Generic)
import Language.Haskell.TH.Syntax

data Basics = Basics
  { basicsPicture :: !Text,
    basicsName :: !Text,
    basicsLabel :: !Text,
    basicsEmail :: !Text,
    basicsPhone :: !Text,
    basicsSummary :: !Text,
    basicsLocation :: !Location,
    basicsProfiles :: ![Profile],
    basicsDateOfBirth :: !Text,
    basicsWebsite :: !Text
  }
  deriving stock (Show, Eq, Ord, Generic, Lift)
  deriving (FromJSON, ToJSON) via (Autodocodec Basics)

instance HasCodec Basics where
  codec =
    object "Basics" $
      Basics
        <$> requiredField' "picture"
          .= basicsPicture
        <*> requiredField' "name"
          .= basicsName
        <*> requiredField' "label"
          .= basicsLabel
        <*> requiredField' "email"
          .= basicsEmail
        <*> requiredField' "phone"
          .= basicsPhone
        <*> requiredField' "summary"
          .= basicsSummary
        <*> requiredField' "location"
          .= basicsLocation
        <*> requiredField' "profiles"
          .= basicsProfiles
        <*> requiredField' "dateOfBirth"
          .= basicsDateOfBirth
        <*> requiredField' "website"
          .= basicsWebsite

data Location = Location
  { locationCity :: !Text,
    locationCountryCode :: !Text,
    locationRegion :: !Text,
    locationPostalCode :: !Text,
    locationCountry :: !Text,
    locationAddress :: !Text
  }
  deriving stock (Show, Eq, Ord, Generic, Lift)
  deriving (FromJSON, ToJSON) via (Autodocodec Location)

instance HasCodec Location where
  codec =
    object "Location" $
      Location
        <$> requiredField' "city"
          .= locationCity
        <*> requiredField' "country-code"
          .= locationCountryCode
        <*> requiredField' "region"
          .= locationRegion
        <*> requiredField' "postal-code"
          .= locationPostalCode
        <*> requiredField' "country"
          .= locationCountry
        <*> requiredField' "address"
          .= locationAddress

data Profile = Profile
  { profileUrl :: !Text,
    profileUsername :: !Text,
    profileNetwork :: !Text
  }
  deriving stock (Show, Eq, Ord, Generic, Lift)
  deriving (FromJSON, ToJSON) via (Autodocodec Profile)

instance HasCodec Profile where
  codec =
    object "Profile" $
      Profile
        <$> requiredField' "url"
          .= profileUrl
        <*> requiredField' "username"
          .= profileUsername
        <*> requiredField' "network"
          .= profileNetwork

data Failure = Failure
  { failureDate :: !Text,
    failureDescription :: !Text,
    failureInstitution :: !Text
  }
  deriving stock (Show, Eq, Ord, Generic, Lift)
  deriving (FromJSON, ToJSON) via (Autodocodec Failure)

instance HasCodec Failure where
  codec =
    object "Failure" $
      Failure
        <$> requiredField' "date"
          .= failureDate
        <*> requiredField' "description"
          .= failureDescription
        <*> requiredField' "institution"
          .= failureInstitution

data Book = Book
  { bookStartDate :: !Text,
    bookTitle :: !Text,
    bookEndDate :: !Text
  }
  deriving stock (Show, Eq, Ord, Generic, Lift)
  deriving (FromJSON, ToJSON) via (Autodocodec Book)

instance HasCodec Book where
  codec =
    object "book" $
      Book
        <$> requiredField' "start-date"
          .= bookStartDate
        <*> requiredField' "title"
          .= bookTitle
        <*> requiredField' "end-date"
          .= bookEndDate

data Talk = Talk
  { talkDate :: !Text,
    talkTitle :: !Text,
    talkLocation :: !Text
  }
  deriving stock (Show, Eq, Ord, Generic, Lift)
  deriving (FromJSON, ToJSON) via (Autodocodec Talk)

instance HasCodec Talk where
  codec =
    object "Talk" $
      Talk
        <$> requiredField' "date"
          .= talkDate
        <*> requiredField' "title"
          .= talkTitle
        <*> requiredField' "location"
          .= talkLocation

data Experience = Experience
  { experienceStartDate :: !Text,
    experienceCompany :: !Text,
    experienceHighlights :: ![Text],
    experiencePosition :: !Text,
    experienceLocation :: !Text,
    experienceVolunteer :: !Bool,
    experienceEndDate :: !(Maybe Text),
    experienceWebsite :: !Text,
    experienceHidden :: !(Maybe Bool),
    experienceRelevantForTechLead :: !(Maybe Bool),
    experienceRelevantForIC :: !(Maybe Bool)
  }
  deriving stock (Show, Eq, Ord, Generic, Lift)
  deriving (FromJSON, ToJSON) via (Autodocodec Experience)

instance HasCodec Experience where
  codec =
    object "Experience" $
      Experience
        <$> requiredField' "start-date"
          .= experienceStartDate
        <*> requiredField' "company"
          .= experienceCompany
        <*> requiredField' "highlights"
          .= experienceHighlights
        <*> requiredField' "position"
          .= experiencePosition
        <*> requiredField' "location"
          .= experienceLocation
        <*> requiredField' "volunteer"
          .= experienceVolunteer
        <*> requiredField' "end-date"
          .= experienceEndDate
        <*> requiredField' "website"
          .= experienceWebsite
        <*> optionalField' "hidden"
          .= experienceHidden
        <*> optionalField' "relevant-for-technical-leader"
          .= experienceRelevantForTechLead
        <*> optionalField' "relevant-for-individual-contributor"
          .= experienceRelevantForIC

data Language = Language
  { languageLanguage :: !Text,
    languageFluency :: !Text
  }
  deriving stock (Show, Eq, Ord, Generic, Lift)
  deriving (FromJSON, ToJSON) via (Autodocodec Language)

instance HasCodec Language where
  codec =
    object "Language" $
      Language
        <$> requiredField' "language"
          .= languageLanguage
        <*> requiredField' "fluency"
          .= languageFluency

data Education = Education
  { educationStartDate :: !Text,
    educationInstitution :: !Text,
    educationArea :: !Text,
    educationStudyType :: !Text,
    educationNote :: !Text,
    educationEndDate :: !Text,
    educationCourses :: ![Text]
  }
  deriving stock (Show, Eq, Ord, Generic, Lift)
  deriving (FromJSON, ToJSON) via (Autodocodec Education)

instance HasCodec Education where
  codec =
    object "Education" $
      Education
        <$> requiredField' "start-date"
          .= educationStartDate
        <*> requiredField' "institution"
          .= educationInstitution
        <*> requiredField' "area"
          .= educationArea
        <*> requiredField' "study-type"
          .= educationStudyType
        <*> requiredField' "note"
          .= educationNote
        <*> requiredField' "end-date"
          .= educationEndDate
        <*> requiredField' "courses"
          .= educationCourses

data Product = Product
  { productName :: !Text,
    productDescription :: !Text,
    productStack :: !Text,
    productWebsite :: !Text,
    productHighlights :: ![Text]
  }
  deriving stock (Show, Eq, Ord, Generic, Lift)
  deriving (FromJSON, ToJSON) via (Autodocodec Product)

instance HasCodec Product where
  codec =
    object "Product" $
      Product
        <$> requiredField' "name"
          .= productName
        <*> requiredField' "description"
          .= productDescription
        <*> requiredField' "stack"
          .= productStack
        <*> requiredField' "website"
          .= productWebsite
        <*> requiredField' "highlights"
          .= productHighlights

data Software = Software
  { softwareLanguage :: !Text,
    softwareTitle :: !Text,
    softwareDescription :: !Text,
    softwareContribution :: !Text
  }
  deriving stock (Show, Eq, Ord, Generic, Lift)
  deriving (FromJSON, ToJSON) via (Autodocodec Software)

instance HasCodec Software where
  codec =
    object "Software" $
      Software
        <$> requiredField' "language"
          .= softwareLanguage
        <*> requiredField' "title"
          .= softwareTitle
        <*> requiredField' "description"
          .= softwareDescription
        <*> requiredField' "contribution"
          .= softwareContribution
