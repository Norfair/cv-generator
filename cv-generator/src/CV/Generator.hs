{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module CV.Generator where

import CV.Generator.Cvs
import CV.Generator.Widget
import Control.Monad
import qualified Data.ByteString as SB
import qualified Data.ByteString.Lazy as LB
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import Path
import Path.IO
import System.Environment
import System.Exit
import System.IO
import System.Process.Typed

cvGenerator :: IO ()
cvGenerator = do
  args <- getArgs
  let mCVname = listToMaybe args
  let cvsToBuild = case mCVname of
        Nothing -> allCvs
        Just name -> filter ((== name) . fst) allCvs
  autoDir <- resolveDir' "auto"
  mapM_ (uncurry $ buildCV autoDir) cvsToBuild

buildCV :: Path Abs Dir -> String -> CV -> IO ()
buildCV autoDir name CV {..} = do
  pdf <- buildLatexFile name (completeTemplate cvWidget) autoDir
  forM_ cvLengthCheck $ \lc -> lengthCheck pdf lc

lengthCheck :: Path Abs File -> Word -> IO ()
lengthCheck fp maxLen = do
  putStrLn $ unwords ["Checking that", fromAbsFile fp, "is no longer than", show maxLen, "pages."]
  o <-
    readProcessInterleaved_ $
      setStdout inherit $
        setStderr inherit $
          shell $
            unwords ["pdftk", fromAbsFile fp, " dump_data", "|", "grep", "NumberOfPages", "|", "sed", "s/[^0-9]*//"]
  let pages :: Word
      pages = read (T.unpack (TE.decodeUtf8 (LB.toStrict o)))
  when (pages > maxLen) $ die $ unwords ["Pdf too long:", fromAbsFile fp, "namely", show pages, "pages"]

buildLatexFile :: String -> Text -> Path Abs Dir -> IO (Path Abs File)
buildLatexFile name contents autoDir = do
  let texFileName = name <> ".tex"
  let pdfFileName = name <> ".pdf"
  let autoSubdirName = name
  autoSubdir <- resolveDir autoDir autoSubdirName
  texFilePath <- resolveFile autoSubdir texFileName
  pdfFilePath <- resolveFile autoSubdir pdfFileName
  ensureDir $ parent texFilePath
  SB.writeFile (fromAbsFile texFilePath) (TE.encodeUtf8 contents)
  putStrLn $ unwords ["Building", show name, "..."]
  let pc =
        setWorkingDir (fromAbsDir (parent texFilePath)) $
          shell $
            unwords
              [ "latexmk",
                "-pdf",
                texFileName,
                "-interaction=nonstopmode",
                "-f-"
              ]
  (ec, output) <- readProcessInterleaved pc
  case ec of
    ExitSuccess -> do
      putStrLn $ unwords ["Building", show name, "succeeded."]
      pure pdfFilePath
    ExitFailure _ -> do
      LB.hPut stderr output
      die $ unwords ["Building", show name, "failed."]
