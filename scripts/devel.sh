#!/usr/bin/env bash
set -ex

stack install :cv-generator \
  --file-watch --watch-all \
  --exec='scripts/generate.sh' \
  --ghc-options="-freverse-errors -DDEVELOPMENT -O0" \
  --fast $@

